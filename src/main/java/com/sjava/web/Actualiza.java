package com.sjava.web;

import java.util.Date;

public class Actualiza {

    static Date fechahoy = new Date();
    static String hoy = MiLib.DatetoString(fechahoy);

    public static void actualiza() {

        for (Oferta oferta : OfertaController.getAll()) {
            if (oferta.pedidosAlcanzados() || oferta.vencida()) {
                for (Pedido pedido : PedidoController.getAllByOferta(oferta.getId())) {
                    PedidoController.convierteEnCompra(pedido);
                }
            }
        }
    }
}
