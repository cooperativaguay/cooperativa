package com.sjava.web;

import java.util.Date;

public class Compra {

    private int id;
    private int pedidoid;
    private int ofertaid;
    private int consumidorid;
    private int proveedorid;
    private Date fecha = new Date();
    private String direccion;
    private float precio;
    
    
    
    

    public Compra (int id, int pedidoid, int ofertaid, int consumidorid, int proveedorid, Date fecha, String direccion, float precio) {
        this.id = id;
        this.pedidoid= proveedorid;
        this.ofertaid = ofertaid;
        this.consumidorid = consumidorid;
        this.proveedorid = proveedorid;
        this.fecha = fecha;
        this.direccion = direccion;
        this.precio = precio;
    

    }

    public Compra ( int pedidoid, int ofertaid, int consumidorid, int proveedorid, Date fecha, String direccion, float precio) {

        this.pedidoid= pedidoid;
        this.ofertaid = ofertaid;
        this.consumidorid = consumidorid;
        this.proveedorid = proveedorid;
        this.fecha = fecha;
        this.direccion = direccion;
        this.precio = precio;
    

    }

    public int getPedidoid() {
        return this.pedidoid;
    }

    protected void setPedidoid(int pedidoid){
        this.pedidoid = pedidoid;
    }

    public int getOfertaid() {
        return this.ofertaid;
    }

    protected void setOfertaid(int ofertaid){
        this.ofertaid = ofertaid;
    }

    public int getConsumidorid() {
        return this.consumidorid;
    }

    protected void setConsumidorid(int consumidorid){
        this.consumidorid = consumidorid;
    }

   
    public int getProveedorid() {
        return this.proveedorid;
    }

    protected void setProveedorid(int proveedorid){
        this.proveedorid = proveedorid;
    }

    public Date getFecha(){
        return this.fecha;
    }
    
    protected void setFecha(Date fecha){
        this.fecha = fecha;
    }
 
    public String getDireccion(){
        return this.direccion;
    }
    
    protected void setDireccion(String direccion){
        this.direccion = direccion;
    }

    public float getPrecio(){
        return this.precio;
    }
    
    protected void setPrecio(Float precio){
        this.precio = precio;
    }

   

    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }


  
  
}