package com.sjava.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class CompraController {
  
    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "compras";
	private static final String KEY = "id";


    // getAll devuelve todos los registros de la tabla
    public static List<Compra> getAll(){
        
        List<Compra> listaCompras = new ArrayList<Compra>();
        String sql = String.format("select %s, pedidos_id, ofertas_id, consumidores_id, proveedores_id, fecha, direccion, precio from %s", KEY, TABLE);
		//String sql = "select id,nombre,password from "+TABLE;
		System.out.println(sql);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Compra u = new Compra (
                    rs.getInt(KEY),
                    rs.getInt("pedidos_id"),
                    rs.getInt("ofertas_id"),
                    rs.getInt("consumidores_id"),
                    rs.getInt("proveedores_id"),
                    rs.getDate("fecha"),
                    rs.getString("direccion"),
                    rs.getFloat("precio"));
                    

                listaCompras.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaCompras;

    }

    //getId devuelve un registro
    public static Compra getId(int id){
        Compra u = null;
        String sql = String.format("select %s, pedidos_id, ofertas_id, consumidores_id, proveedores_id, fecha, direccion, precio from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                 u = new Compra (
                    rs.getInt(KEY),
                    rs.getInt("pedidos_id"),
                    rs.getInt("ofertas_id"),
                    rs.getInt("consumidores_id"),
                    rs.getInt("proveedores_id"),
                    rs.getDate("fecha"),
                    rs.getString("direccion"),
                    rs.getFloat("precio"));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo anade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Compra al) {
        String sql;
        if (al.getId()>0) {
            sql = String.format("UPDATE %s set pedidos_id=?, ofertas_id=?, consumidores_id=?, proveedores_id=?, fecha=?, direccion=?, precio=? where %s=%d", TABLE, KEY, al.getId());
        }else {
            sql = String.format("INSERT INTO %s (pedidos_id, ofertas_id, consumidores_id, proveedores_id, fecha, direccion, precio) VALUES (?,?,?,?,?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setInt(1, al.getPedidoid());
            pstmt.setInt(2, al.getOfertaid());
            pstmt.setInt(3, al.getConsumidorid());
            pstmt.setInt(4, al.getProveedorid());

            long fe = al.getFecha().getTime();
            java.sql.Date fecha = new java.sql.Date(fe);
            pstmt.setDate(5, fecha);

        
            pstmt.setString(6, al.getDireccion());
            pstmt.setFloat(7, al.getPrecio());


            pstmt.executeUpdate();
            
        if (al.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}