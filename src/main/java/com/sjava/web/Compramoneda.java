package com.sjava.web;

import java.util.Date;

public class Compramoneda {

    private int id;
    private int importe;
    private Date fecha;
    private String consumidoresid;

    public Compramoneda(int importe, Date fecha, String consumidoresid) {
        this.importe = importe;
        this.fecha = fecha;
        this.consumidoresid = consumidoresid;
    }

    public Compramoneda(int id, int importe, Date fecha, String consumidoresid) {
        this.id = id;
        this.importe = importe;
        this.fecha = fecha;
        this.consumidoresid = consumidoresid;
    }

    public int getImporte() {
        return this.importe;
    }

    protected void setImporte(int importe) {
        this.importe = importe;
    }

    public Date getFecha() {
        return this.fecha;
    }

    protected void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getConsumidoresid() {
        return this.consumidoresid;
    }

    protected void setConsumidoresid(String consumidoresid) {
        this.consumidoresid = consumidoresid;
    }

    public int getId() {
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }
}