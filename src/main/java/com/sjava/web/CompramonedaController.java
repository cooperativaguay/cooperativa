package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class CompramonedaController {

    // constantes utilizadas en las ordenes sql
    private static final String TABLE = "provincia";
    private static final String KEY = "id";

    // getAll devuelve todos los registros de la tabla
    public static List<Compramoneda> getAll() {

        List<Compramoneda> listaCompramonedas = new ArrayList<Compramoneda>();
        String sql = String.format("select %s,importe, fecha, consumidoresid from %s", KEY, TABLE);
        // String sql = "select id,nombre,password from "+TABLE;
        System.out.println(sql);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Compramoneda u = new Compramoneda(rs.getInt(KEY), rs.getInt("importe"), rs.getDate("fecha"),
                        rs.getString("consumidoresid"));

                listaCompramonedas.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaCompramonedas;

    }

    // getId devuelve un registro
    public static Compramoneda getId(int id) {
        Compramoneda u = null;
        String sql = String.format("select %s,importe, fecha, consumidoresid from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Compramoneda(rs.getInt(KEY), rs.getInt("importe"), rs.getDate("fecha"),
                        rs.getString("consumidoresid"));

            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }

    // save guarda un alumno
    // si es nuevo (id==0) lo anade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Compramoneda al) {
        String sql;
        if (al.getId() > 0) {
            sql = String.format("UPDATE %s set importe=?, fecha=?, consumidoresid=? where %s=%d", TABLE, KEY,
                    al.getId());
        } else {
            sql = String.format("INSERT INTO %s (importe, fecha, consumidores_id) VALUES (?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setInt(1, al.getImporte());

            long f = al.getFecha().getTime();
            java.sql.Date fecha = new java.sql.Date(f);
            pstmt.setDate(2, fecha);

            pstmt.setString(3, al.getConsumidoresid());

            pstmt.executeUpdate();

            if (al.getId() == 0) {
                // usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }

    // removeId elimina alumno por id
    public static void removeId(int id) {
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}