package com.sjava.web;

public class Consumidor {

    private int id;
    private String nombre;
    private String contrasena;
    private String email;
    private String telefono;
    private String direccion;
    private float saldo;

    public Consumidor(String nombre, String contrasena, String email, 
        String telefono, String direccion, float saldo) {
        this.nombre = nombre;
        this.contrasena = contrasena;
        this.email = email;
        this.telefono = telefono;
        this.direccion = direccion;
        this.saldo = saldo;
    }

    public Consumidor(int id, String nombre, String contrasena, String email, 
        String telefono, String direccion, float saldo) {
        this.id = id;
        this.nombre = nombre;
        this.contrasena = contrasena;
        this.email = email;
        this.telefono = telefono;
        this.direccion = direccion;
        this.saldo = saldo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getContrasena(){
        return this.contrasena;
    }
    
    public void setContrasena(String contrasena){
        this.contrasena = contrasena;
    }

    public String getEmail(){
        return this.email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getTelefono(){
        return this.telefono;
    }
    
    public void setTelefono(String telefono){
        this.telefono = telefono;
    }

    public String getDireccion(){
        return this.direccion;
    }
    
    public void setDireccion(String direccion){
        this.direccion = direccion;
    }

    public float getSaldo(){
        return this.saldo;
    }
    
    public void setSaldo(Float saldo){
        this.saldo = saldo;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id=id;
    }


   

    }

 
  
