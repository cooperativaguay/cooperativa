package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class ConsumidorController {
  
    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "consumidores";
	private static final String KEY = "id";


    // getAll devuelve todos los registros de la tabla
    public static List<Consumidor> getAll(){
        
        List<Consumidor> listaConsumidores = new ArrayList<Consumidor>();
		String sql = String.format("select %s,nombre, contrasena, email, telefono, direccion, saldo from %s", KEY, TABLE);
		//String sql = "select id,nombre,password from "+TABLE;
		System.out.println(sql);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Consumidor u = new Consumidor(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getString("contrasena"),
                    rs.getString("email"),
                    rs.getString("telefono"),
                    rs.getString("direccion"),
                    rs.getFloat("saldo"));

                listaConsumidores.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaConsumidores;

    }

    //getId devuelve un registro
    public static Consumidor getId(int id){
        Consumidor u = null;
        String sql = String.format("select %s,nombre, contrasena, email,telefono, direccion, saldo from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Consumidor(
                (Integer) rs.getObject(1),
                (String) rs.getObject(2),
                (String) rs.getObject(3),
                (String) rs.getObject(4),
                (String) rs.getObject(5),
                (String) rs.getObject(6),
                (Float) rs.getObject(7)
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo anade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Consumidor al) {
        String sql;
        if (al.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?,  email=?, telefono=?, direccion=?, saldo=? where %s=%d", TABLE, KEY, al.getId());
        }else {
            sql = String.format("INSERT INTO %s (nombre, email, telefono, direccion, saldo, contrasena) VALUES (?,?,?,?,?,password(?))", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, al.getNombre());
            
            pstmt.setString(2, al.getEmail());
            pstmt.setString(3, al.getTelefono());
            pstmt.setString(4, al.getDireccion());
            pstmt.setFloat(5, al.getSaldo());

            if (al.getId()==0) {
                pstmt.setString(6, al.getContrasena());
            }
            
            pstmt.executeUpdate();
        if (al.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static int checkLogin(String nombre, String password){
		int resId = -1;
		
		String sql = String.format("select %s from %s where nombre=? and contrasena=password(?)", KEY, TABLE);
		try (Connection conn = DBConn.getConn();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {
			
			pstmt.setString(1, nombre);
			pstmt.setString(2, password);

			ResultSet rs = pstmt.executeQuery();
		
			if (rs.next()) {
				 resId = rs.getInt(1);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return resId;
    }


    public static void cambiaSaldo (Consumidor consumidor, boolean compra, float cantidad) {
        float saldoactual = consumidor.getSaldo();
        if (compra) {
           float saldocambio = saldoactual + cantidad;
           consumidor.setSaldo(saldocambio);
          save(consumidor);
        }
        else {
            float saldocambio = saldoactual - cantidad;
            consumidor.setSaldo(saldocambio);
            save(consumidor);
         }

    }



}