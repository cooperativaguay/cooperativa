package com.sjava.web;

public class Consumidoreshasprovincias {

    private int consumidorid;
    private int provinciaid;
    

    public Consumidoreshasprovincias(int consumidorid, int provinciaid) {
        this.consumidorid = consumidorid;
        this.provinciaid = provinciaid;
    }


    public int getConsumidorid(){
        return this.consumidorid;
    }

    protected void setConsumidorid(int consumidorid){
        this.consumidorid=consumidorid;
    }

    public int getProvinciaid(){
        return this.provinciaid;
    }

    protected void setProvinciaid(int provinciaid){
        this.provinciaid=provinciaid;
    }


  
}