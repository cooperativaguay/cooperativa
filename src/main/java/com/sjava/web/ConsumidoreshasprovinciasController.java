package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class ConsumidoreshasprovinciasController {
  
    // constantes utilizadas en las ordenes sql
    private static final String TABLE = "consumidores_has_provincias";
    private static final String ID1 = "consumidores_id";
    private static final String ID2 = "provincias_id";
    



    // getAll devuelve todos los registros de la tabla
    public static List<Consumidoreshasprovincias> getAll(){
        
        List<Consumidoreshasprovincias> listaConsumidoreshasprovincias = new ArrayList<Consumidoreshasprovincias>();
		String sql = String.format("select consumidores_id, provincias_id from %s", TABLE);
		//String sql = "select id,nombre,password from "+TABLE;
		System.out.println(sql);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Consumidoreshasprovincias u = new Consumidoreshasprovincias(
                    rs.getInt("consumidores_id"),
                    rs.getInt("provincias_id"));

                listaConsumidoreshasprovincias.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaConsumidoreshasprovincias;

    }

    
    public static void save(Consumidoreshasprovincias al) {
        String sql;

        for (Consumidoreshasprovincias p: getAll()) {

            if (al.getConsumidorid() == p.getConsumidorid() && al.getProvinciaid() == p.getProvinciaid()) {
                return;
            }
           
        else {
            sql = String.format("INSERT INTO %s (%s, %s) VALUES (?,?)", TABLE, ID1, ID2);
        
            try (Connection conn = DBConn.getConn();
                    PreparedStatement pstmt = conn.prepareStatement(sql);
                    Statement stmt = conn.createStatement()) {
                        
                pstmt.setInt(1, al.getConsumidorid());
                pstmt.setInt(2, al.getProvinciaid());
    
                pstmt.executeUpdate();
                
            
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        }}





    // removeId elimina registros
    public static void removeId(int consumidorid, int provinciaid){
        String sql = String.format("DELETE FROM %s where %s=%d & %s=%d", TABLE, ID1, consumidorid, ID2, provinciaid);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}