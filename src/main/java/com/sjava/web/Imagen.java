package com.sjava.web;

public class Imagen {

    private int id;
    private String url;
  

    public Imagen (int id, String url) {
        this.id = id;
        this.url = url;
      
    }

    public Imagen (String url) {
        this.url = url;
      
    }

    public String geturl() {
        return this.url;
    }

    protected void seturl(String url) {
        this.url = url;
    }


    public int getId() {
        return this.id;
    }

    protected void setId(int id) {
        this.id = id;
    }

}