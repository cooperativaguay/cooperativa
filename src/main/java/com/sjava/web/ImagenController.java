package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class ImagenController   {
  
    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "imagenes";
	private static final String KEY = "id";


    // getAll devuelve todos los registros de la tabla
    public static List<Imagen> getAll(){
        
        List<Imagen> listaImagenes = new ArrayList<Imagen>();
		String sql = String.format("select %s,url from %s", KEY, TABLE);
		//String sql = "select id,url,password from "+TABLE;
		System.out.println(sql);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Imagen u = new Imagen (
                    rs.getInt(KEY),
                    rs.getString("url"));

                listaImagenes.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
        return listaImagenes;

    }

    //getId devuelve un registro
    public static Imagen getId(int id){
        Imagen u = null;
        String sql = String.format("select %s,url from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Imagen(
                    rs.getInt(KEY),
                    rs.getString("url"));
                    
                }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo anade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Imagen al) {
        String sql;
        if (al.getId()>0) {
            sql = String.format("UPDATE %s set url=? where %s=%d", TABLE, KEY, al.getId());
        }else {
            sql = String.format("INSERT INTO %s (url) VALUES (?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, al.geturl());

    
            pstmt.executeUpdate();
        if (al.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}