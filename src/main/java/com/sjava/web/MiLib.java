package com.sjava.web;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MiLib{

    /*
    método que a partir de una fecha en formato aaaa-mm-dd devuelve
    un objeto java.util.Date
    sirve por ejemplo para convertir una fecha recibida desde un formulario web
    a una fecha adecuada para setDate de preparedstatement
    */
    public static Date stringToDate(String sdata){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        Date theDate = null;
            try {
                theDate = df.parse(sdata);
            } catch (ParseException e) {
                //e.printStackTrace();
            }
        return theDate;
    }



public static String DatetoString(Date fecha){
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
   return df.format(fecha);

}
}


