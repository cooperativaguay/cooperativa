package com.sjava.web;

import java.util.Date;

public class Oferta {

    private int id;
    private int proveedorid;
    private int productoid;
    private String imagenid;
    private int cantidadmin;
    private int cantidadmax;
    private float precio;
    private String descripcion;
    private Date fechaenvio;
    private Date fechatope;
    

    public Oferta(int id, int proveedorid, int productoid, String imagenid, int cantidadmin, 
     int cantidadmax, float precio, String descripcion, Date fechaenvio, Date fechatope) {
        this.id = id;
        this.proveedorid = proveedorid;
        this.productoid = productoid;
        this.imagenid = imagenid;
        this.cantidadmin = cantidadmin;
        this.cantidadmax = cantidadmax;
        this.precio = precio;
        this.descripcion = descripcion;
        this.fechaenvio = fechaenvio;
        this.fechatope = fechatope;

    }

    public Oferta(int proveedorid, int productoid, String imagenid, int cantidadmin, 
    int cantidadmax, float precio, String descripcion, Date fechaenvio, Date fechatope) {
       this.proveedorid = proveedorid;
       this.productoid = productoid;
       this.imagenid = imagenid;
       this.cantidadmin = cantidadmin;
       this.cantidadmax = cantidadmax;
       this.precio = precio;
       this.descripcion = descripcion;
       this.fechaenvio = fechaenvio;
       this.fechatope = fechatope;

   }

    public int getProveedorid() {
        return this.proveedorid;
    }

    protected void setProveedorid(int proveedorid){
        this.proveedorid = proveedorid;
    }

    public int getProductoid() {
        return this.productoid;
    }

    protected void setProductoid(int productoid){
        this.productoid = productoid;
    }


    public String getImagenid() {
        return this.imagenid;
    }

    protected void setImagenid(String imagenid){
        this.imagenid = imagenid;
    }

    public int getCantidadmin() {
        return this.cantidadmin;
    }

    protected void setCantidadmin(int cantidadmin){
        this.cantidadmin = cantidadmin;
    }

    public int getCantidadmax() {
        return this.cantidadmax;
    }

    protected void setCantidadmax(int cantidadmax){
        this.cantidadmax = cantidadmax;
    }

    public float getPrecio(){
        return this.precio;
    }
    
    protected void setPrecio(Float precio){
        this.precio = precio;
    }

    public String getDescripion(){
        return this.descripcion;
    }
    
    protected void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }

    public Date getFechaenvio(){
        return this.fechaenvio;
    }
    
    protected void setFechaenvio(Date fechaenvio){
        this.fechaenvio = fechaenvio;
    }

    public Date getFechatope(){
        return this.fechatope;
    }
    
    protected void setFechatope(Date fechatope){
        this.fechatope = fechatope;
    }

    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }


  
    public boolean pedidosAlcanzados() {
        int cantidadtotal = 0;
        for (Pedido p : PedidoController.getAllByOferta(this.id)) {


                cantidadtotal = cantidadtotal + p.getCantidad();
            }

            if (this.cantidadmax<=cantidadtotal) {
                return true;
            }
           else
           {return false;}
    }


    public int cantidadPedida() {
        int cantidadpedida = 0;
        for (Pedido p : PedidoController.getAllByOferta(this.id)) {


                cantidadpedida = cantidadpedida + p.getCantidad();
            }

          return cantidadpedida;
    }
  


public boolean vencida() {
    Date hoy = new Date();
    Date vencida = this.getFechatope();

    if (hoy.equals(vencida)) {
        return true;
    }

    return false;

    }

public String getNombreProducto(){
    return ProductoController.getId(this.productoid).getNombre();
}

public String getNombreProveedor(){
    return ProductoController.getId(this.proveedorid).getNombre();
}

   

}
