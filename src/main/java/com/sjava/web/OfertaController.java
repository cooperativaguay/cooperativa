package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class OfertaController {

    // constantes utilizadas en las ordenes sql
    private static final String TABLE = "ofertas";
    private static final String KEY = "id";

    // getAll devuelve todos los registros de la tabla
    public static List<Oferta> getAll() {

        List<Oferta> listaOfertas = new ArrayList<Oferta>();
        String sql = String.format(
                "select %s, proveedores_id, productos_id, imagenes_id, cantidad_min, cantidad_max, precio, descripcion, fecha_envio, fecha_tope from %s",
                KEY, TABLE);
        // String sql = "select id,nombre,password from "+TABLE;
        System.out.println(sql);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Oferta u = new Oferta(rs.getInt(KEY), rs.getInt("proveedores_id"), rs.getInt("productos_id"),
                        rs.getString("imagenes_id"), rs.getInt("cantidad_min"), rs.getInt("cantidad_max"),
                        rs.getFloat("precio"), rs.getString("descripcion"), rs.getDate("fecha_envio"),
                        rs.getDate("fecha_tope"));

                listaOfertas.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaOfertas;

    }

    // getId devuelve un registro
    public static Oferta getId(int id) {
        Oferta u = null;
        String sql = String.format(
                "select %s, proveedores_id, productos_id, imagenes_id, cantidad_min, cantidad_max, precio, descripcion, fecha_envio, fecha_tope from %s where %s=%d",
                KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Oferta(rs.getInt(KEY), rs.getInt("proveedores_id"), rs.getInt("productos_id"),
                        rs.getString("imagenes_id"), rs.getInt("cantidad_min"), rs.getInt("cantidad_max"),
                        rs.getFloat("precio"), rs.getString("descripcion"), rs.getDate("fecha_envio"),
                        rs.getDate("fecha_tope"));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }

    // save guarda un alumno
    // si es nuevo (id==0) lo anade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Oferta al) {
        String sql;
        if (al.getId() > 0) {
            sql = String.format(
                    "UPDATE %s set proveedores_id=?, productos_id=?, imagenes_id=?, cantidad_min=?, cantidadmax=?, precio=?, descripcion=?, fecha_envio=?, fecha_tope=? where %s=%d",
                    TABLE, KEY, al.getId());
        } else {
            sql = String.format(
                    "INSERT INTO %s (proveedores_id, productos_id, imagenes_id, cantidad_min, cantidad_max, precio, descripcion, fecha_envio, fecha_tope) VALUES (?,?,?,?,?,?,?,?,?)",
                    TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setInt(1, al.getProveedorid());
            pstmt.setInt(2, al.getProductoid());
            pstmt.setString(3, al.getImagenid());
            pstmt.setInt(4, al.getCantidadmin());
            pstmt.setInt(5, al.getCantidadmax());
            pstmt.setFloat(6, al.getPrecio());
            pstmt.setString(7, al.getDescripion());

            long fe = al.getFechaenvio().getTime();
            java.sql.Date fechaenvio = new java.sql.Date(fe);
            pstmt.setDate(8, fechaenvio);

            long ft = al.getFechatope().getTime();
            java.sql.Date fechatope = new java.sql.Date(ft);
            pstmt.setDate(9, fechatope);

            pstmt.executeUpdate();

            if (al.getId() == 0) {
                // usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }

    // removeId elimina alumno por id
    public static void removeId(int id) {
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static boolean convierteEnPedido(Oferta oferta, int cantidad, int consumidorid) {
        int ofertaid = oferta.getId();
        float precio = cantidad * oferta.getPrecio();
        Consumidor cons = ConsumidorController.getId(consumidorid);
        float saldo = cons.getSaldo();

        int cantidadnueva = oferta.cantidadPedida() + cantidad;
        if (cantidadnueva > oferta.getCantidadmax() | precio>saldo) {
            return false;
        }

        else {
            Pedido pedido = new Pedido(ofertaid, consumidorid, cantidad, precio);
            PedidoController.save(pedido);

            return true;

        }
    }

    // getAll devuelve todos los registros de la tabla
    public static List<Oferta> getAllProvincia(int idprovincia) {

        List<Oferta> listaOfertas = new ArrayList<Oferta>();
        String sql = String.format(
                "select o.* from provincias_has_ofertas po left join ofertas o on o.id=po.ofertas_id where po.provincias_id=%d",
                idprovincia);
        // String sql = "select id,nombre,password from "+TABLE;
        System.out.println(sql);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Oferta u = new Oferta(rs.getInt(KEY), rs.getInt("proveedores_id"), rs.getInt("productos_id"),
                        rs.getString("imagenes_id"), rs.getInt("cantidad_min"), rs.getInt("cantidad_max"),
                        rs.getFloat("precio"), rs.getString("descripcion"), rs.getDate("fecha_envio"),
                        rs.getDate("fecha_tope"));

                listaOfertas.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaOfertas;

    }

}