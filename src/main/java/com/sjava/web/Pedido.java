package com.sjava.web;

public class Pedido {

    private int id;
    private int ofertaid;
    private int consumidorid;
    private int cantidad;
    private float precio;

    

    public Pedido (int id, int ofertaid, int consumidorid, int cantidad, float precio) {
        this.id = id;
        this.ofertaid = ofertaid;
        this.consumidorid = consumidorid;
        this.cantidad = cantidad;
        this.precio = precio;

    }

    public Pedido (int ofertaid, int consumidorid, int cantidad, float precio) {

        this.ofertaid = ofertaid;
        this.consumidorid = consumidorid;
        this.cantidad = cantidad;
        this.precio = precio;

    }

    public int getOfertaid() {
        return this.ofertaid;
    }

    protected void setOfertaid(int ofertaid){
        this.ofertaid = ofertaid;
    }

    public int getConsumidorid() {
        return this.consumidorid;
    }

    protected void setConsumidorid(int consumidorid){
        this.consumidorid = consumidorid;
    }


    public int getCantidad() {
        return this.cantidad;
    }

    protected void setCantidad(int cantidad){
        this.cantidad = cantidad;
    }


    public float getPrecio(){
        return this.precio;
    }
    
    protected void setPrecio(Float precio){
        this.precio = precio;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }

    public String getNombreProducto(){
        return OfertaController.getId(this.ofertaid).getNombreProducto();
    }

    public String getNombreProveedor(){
        return OfertaController.getId(this.ofertaid).getNombreProveedor();
    }
  
  
}