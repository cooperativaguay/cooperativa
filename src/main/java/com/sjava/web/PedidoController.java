package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import com.mysql.jdbc.Connection;

public class PedidoController {

    // constantes utilizadas en las ordenes sql
    private static final String TABLE = "pedidos";
    private static final String KEY = "id";

    // getAll devuelve todos los registros de la tabla
    public static List<Pedido> getAll() {

        List<Pedido> listaPedidos = new ArrayList<Pedido>();
        String sql = String.format("select %s, ofertas_id, consumidores_id, cantidad, precio from %s", KEY, TABLE);
        // String sql = "select id,nombre,password from "+TABLE;
        System.out.println(sql);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Pedido u = new Pedido(rs.getInt(KEY), rs.getInt("ofertas_id"), rs.getInt("consumidores_id"),
                        rs.getInt("cantidad"), rs.getFloat("precio"));

                listaPedidos.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaPedidos;

    }

    // getId devuelve un registro
    public static Pedido getId(int id) {
        Pedido u = null;
        String sql = String.format("select %s, ofertas_id, consumidores_id, cantidad, precio from %s where %s=%d", KEY,
                TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Pedido(rs.getInt(KEY), rs.getInt("ofertas_id"), rs.getInt("consumidores_id"),
                        rs.getInt("cantidad"), rs.getFloat("precio"));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }

    // save guarda un alumno
    // si es nuevo (id==0) lo anade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Pedido al) {
        String sql;
        if (al.getId() > 0) {
            sql = String.format("UPDATE %s set ofertas_id=?, consumidores_id=?, cantidad=?, precio=? where %s=%d",
                    TABLE, KEY, al.getId());
        } else {
            sql = String.format("INSERT INTO %s (ofertas_id, consumidores_id, cantidad, precio) VALUES (?,?,?,?)",
                    TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setInt(1, al.getOfertaid());
            pstmt.setInt(2, al.getConsumidorid());
            pstmt.setInt(3, al.getCantidad());
            pstmt.setFloat(4, al.getPrecio());

            pstmt.executeUpdate();

            if (al.getId() == 0) {
                // usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }

    // removeId elimina alumno por id
    public static void removeId(int id) {
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static List<Pedido> getAllByOferta(int oferta) {

        List<Pedido> listaPedidosbyOferta = new ArrayList<Pedido>();
        String sql = String.format(
                "select %s, ofertas_id, consumidores_id, cantidad, precio from %s WHERE ofertas_id=%d", KEY, TABLE,
                oferta);
        // String sql = "select id,nombre,password from "+TABLE;
        System.out.println(sql);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Pedido u = new Pedido(rs.getInt(KEY), rs.getInt("ofertas_id"), rs.getInt("consumidores_id"),
                        rs.getInt("cantidad"), rs.getFloat("precio"));

                listaPedidosbyOferta.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaPedidosbyOferta;

    }

    public static void convierteEnCompra(Pedido p) {

        int idpedido = p.getId();
        int idoferta = p.getOfertaid();
        int idconsumidor = p.getConsumidorid();
        Consumidor cons = ConsumidorController.getId(idconsumidor);
        Oferta oferta = OfertaController.getId(idoferta);
        int idproveedor = oferta.getProveedorid();
        Proveedor prov = ProveedorController.getId(idproveedor);
        Date hoy = new Date();
        String direccion = cons.getDireccion();
        float precio = p.getPrecio();

        Compra compra = new Compra(idpedido, idoferta, idconsumidor, idproveedor, hoy, direccion, precio);
        CompraController.save(compra);

        ConsumidorController.cambiaSaldo(cons, false, precio);

        ProveedorController.cambiaSaldo(prov, true, precio);
        PedidoController.removeId(idpedido);
    }

    // getAll devuelve todos los registros de la tabla
    public static List<Pedido> getAllConsumidor(int idConsumidor) {

        List<Pedido> listaPedidos = new ArrayList<Pedido>();
        String sql = String.format(
                "select %s, ofertas_id, consumidores_id, cantidad, precio from %s where consumidores_id=%d", KEY, TABLE,
                idConsumidor);
        // String sql = "select id,nombre,password from "+TABLE;
        System.out.println(sql);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Pedido u = new Pedido(rs.getInt(KEY), rs.getInt("ofertas_id"), rs.getInt("consumidores_id"),
                        rs.getInt("cantidad"), rs.getFloat("precio"));

                listaPedidos.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaPedidos;

    }

}
