package com.sjava.web;

public class Producto {

    private int id;
    private int tipoproductoid;
    private String nombre;
  

    public Producto (int id,int tipoproductoid, String nombre) {
        this.id = id;
        this.tipoproductoid = tipoproductoid;
        this.nombre = nombre;
      
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTipoproductoid() {
        return this.tipoproductoid;
    }
    protected void setTipoproductoid(int tipoproductoid) {
        this.tipoproductoid = tipoproductoid;
    }

    public int getId() {
        return this.id;
    }

    protected void setId(int id) {
        this.id = id;
    }

}