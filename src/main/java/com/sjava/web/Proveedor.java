package com.sjava.web;

public class Proveedor {

    private int id;
    private String nombre;
    private int provinciaid;
    private String contrasena;
    private String email;
    private String CIF;
    private String descripcion;
    private Float saldo;
    private String imagen;

    public Proveedor(String nombre, int provinciaid, String contrasena, String email, String cif, String descripcion, Float saldo, String imagen) {
        this.nombre = nombre;
        this.provinciaid = provinciaid;
        this.contrasena = contrasena;
        this.email = email;
        this.CIF = cif;
        this.descripcion = descripcion;
        this.saldo = saldo;
        this.imagen = imagen;
    }

    public Proveedor(int id,String nombre, int provinciaid, String contrasena, String email, 
        String cif, String descripcion, Float saldo, String imagen) {
        this.id = id;
        this.nombre = nombre;
        this.provinciaid = provinciaid;
        this.contrasena = contrasena;
        this.email = email;
        this.CIF = cif;
        this.descripcion = descripcion;
        this.saldo = saldo;
        this.imagen = imagen;
    }


    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getProvinciaid() {
        return this.provinciaid;
    }

    protected void setProvinciaid(int provinciaid) {
        this.provinciaid = provinciaid;
    }

    public String getContrasena() {
        return this.contrasena;
    }

    protected void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getEmail() {
        return this.email;
    }

    protected void setEmail(String email) {
        this.email = email;
    }

    public String getCif() {
        return this.CIF;
    }

    protected void setTelefono(String cif) {
        this.CIF = cif;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    protected void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }



    public float getSaldo() {
        return this.saldo;
    }

    protected void setSaldo(Float saldo) {
        this.saldo = saldo;
    }

    
    public String getImagen() {
        return this.imagen;
    }

    protected void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getId() {
        return this.id;
    }

    protected void setId(int id) {
        this.id = id;
    }

}