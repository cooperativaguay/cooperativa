package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class ProveedorController {
  
    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "proveedores";
	private static final String KEY = "id";


    // getAll devuelve todos los registros de la tabla
    public static List<Proveedor> getAll(){
        
        List<Proveedor> listaProveedores = new ArrayList<Proveedor>();
		String sql = String.format("select %s,nombre, provincias_id, contrasena, email, CIF, descripcion, saldo, imagen from %s", KEY, TABLE);
		//String sql = "select id,nombre,password from "+TABLE;
		System.out.println(sql);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Proveedor u = new Proveedor(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getInt("provincias_id"),
                    rs.getString("contrasena"),
                    rs.getString("email"),
                    rs.getString("CIF"),
                    rs.getString("descripcion"),
                    rs.getFloat("saldo"),
                    rs.getString("imagen"));

                listaProveedores.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
        return listaProveedores;

    }

    //getId devuelve un registro
    public static Proveedor getId(int id){
        Proveedor u = null;
        String sql = String.format("select %s,nombre, contrasena, provincias_id, email, CIF, descripcion, saldo, imagen from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Proveedor(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getInt("provincias_id"),
                    rs.getString("contrasena"),
                    rs.getString("email"),
                    rs.getString("CIF"),
                    rs.getString("descripcion"),
                    rs.getFloat("saldo"),
                    rs.getString("imagen"));
                    
                }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo anade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Proveedor al) {
        String sql;
        if (al.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, provincias_id=?,  email=?, CIF=?, descripcion=?, saldo=?, imagen=? where %s=%d", TABLE, KEY, al.getId());
        }else {
            sql = String.format("INSERT INTO %s (nombre, provincias_id, email, CIF, descripcion, saldo, imagen, contrasena) VALUES (?,?,?,?,?,?,?,password(?))", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, al.getNombre());
            pstmt.setInt(2, al.getProvinciaid());
           
            pstmt.setString(3, al.getEmail());
            pstmt.setString(4, al.getCif());
            pstmt.setString(5, al.getDescripcion());
            pstmt.setFloat(6, al.getSaldo());
            pstmt.setString(7, al.getImagen());

            if (al.getId()==0) {
            pstmt.setString(8, al.getContrasena());
            }

            pstmt.executeUpdate();
        if (al.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static int checkLogin(String CIF, String password){
		int resId = -1;
		
		String sql = String.format("select %s from %s where CIF=? and contrasena=password(?)", KEY, TABLE);
		try (Connection conn = DBConn.getConn();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setString(1, CIF);
			pstmt.setString(2, password);

			ResultSet rs = pstmt.executeQuery();
		
			if (rs.next()) {
				 resId = rs.getInt(1);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return resId;
    }



    public static void cambiaSaldo (Proveedor proveedor, boolean compra, float cantidad) {
        float saldoactual = proveedor.getSaldo();
        if (compra) {
           float saldocambio = saldoactual + cantidad;
           proveedor.setSaldo(saldocambio);
          save(proveedor);
        }
        else {
            float saldocambio = saldoactual - cantidad;
            proveedor.setSaldo(saldocambio);
            save(proveedor);
         }

    }

}