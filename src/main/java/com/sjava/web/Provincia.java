package com.sjava.web;

public class Provincia {

    private int id;
    private String nombre;
  

    public Provincia (int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
      
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public int getId() {
        return this.id;
    }

    protected void setId(int id) {
        this.id = id;
    }

}