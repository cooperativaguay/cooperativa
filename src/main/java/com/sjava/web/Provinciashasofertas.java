package com.sjava.web;

public class Provinciashasofertas {

    private int provinciaid;
    private int ofertaid;

    public Provinciashasofertas(int provinciaid, int ofertaid) {
        this.provinciaid = provinciaid;
        this.ofertaid = ofertaid;

    }

    public int getProvinciaid() {
        return this.provinciaid;
    }

    protected void setProvinciaid(int provinciaid) {
        this.provinciaid = provinciaid;
    }

    public int getOfertaid() {
        return this.ofertaid;
    }

    protected void setOfertaid(int ofertaid) {
        this.ofertaid = ofertaid;
    }
}
   