package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class ProvinciashasofertasController {
    // constantes utilizadas en las ordenes sql
    private static final String TABLE = "provincias_has_ofertas";
    private static final String ID1 = "provincias_id";
    private static final String ID2 = "ofertas_id";



   // getAll devuelve todos los registros de la tabla
   public static List<Provinciashasofertas> getAll(){
      
       List<Provinciashasofertas> listaProvinciashasofertas = new ArrayList<Provinciashasofertas>();
        String sql = String.format("select provincias_id, ofertas_id from %s", TABLE);
        //String sql = "select id,nombre,password from "+TABLE;
        System.out.println(sql);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Provinciashasofertas u = new Provinciashasofertas(
                   rs.getInt("provincias_id"),
                   rs.getInt("ofertas_id"));

                   listaProvinciashasofertas.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaProvinciashasofertas;

   }

  
   public static void save(Provinciashasofertas al) {
       String sql;

      

          
         
    
        sql = String.format("INSERT INTO %s (%s, %s) VALUES (?,?)", TABLE, ID1, ID2);
      
           try (Connection conn = DBConn.getConn();
                   PreparedStatement pstmt = conn.prepareStatement(sql);
                   Statement stmt = conn.createStatement()) {
                      
               pstmt.setInt(1, al.getProvinciaid());
               pstmt.setInt(2, al.getOfertaid());
  
               pstmt.executeUpdate();
              
          
           } catch (Exception e) {
               System.out.println(e.getMessage());
           }
       }



    



public static void removebyOferta(int idoferta) {
    String sql = String.format("DELETE FROM %s where %s=%d", TABLE,  ID2, idoferta);
    try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
        stmt.executeUpdate(sql);
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
}
}
