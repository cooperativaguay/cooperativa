<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>


<!DOCTYPE html>
<html lang="es-ES">

<head>
	<meta charset="utf-8">
	<title>Canjear monedas</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	 crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
	<link rel="stylesheet" href="/cooperativa/css/compras.css">
</head>


<body style="background-color: rgba(102,255,0,0.329)">
	<%@include file="/parts/barra_consumidor.jsp"%>
	<div class="container">
		<br>
		<div class="row">
			<aside class="col-sm-12">

				<article class="card" style="background-color: beige">
					<h1>
						<center>CANAL DE PAGO</center>
					</h1>
					<div class="card-body p-5">
						<p>
							<center>
								<img src="http://bootstrap-ecommerce.com/main/images/icons/pay-visa.png">
								<img src="http://bootstrap-ecommerce.com/main/images/icons/pay-mastercard.png">
								<img src="http://bootstrap-ecommerce.com/main/images/icons/pay-american-ex.png">
							</center>
						</p>
						<!-- <p class="alert alert-success">Some text success or error</p> -->

						<form role="form">
							<div class="form-group">
								<label for="username">Full name (on the card)</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="fa fa-user"></i>
										</span>
									</div>
									<input type="text" class="form-control" name="username" placeholder="" required="">
								</div>
								<!-- input-group.// -->
							</div>
							<!-- form-group.// -->

							<div class="form-group">
								<label for="cardNumber">Card number</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="fa fa-credit-card"></i>
										</span>
									</div>
									<input type="text" class="form-control" name="cardNumber" placeholder="">
								</div>
								<!-- input-group.// -->
							</div>
							<!-- form-group.// -->

							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<label>
											<span class="hidden-xs">Expiration</span>
										</label>
										<div class="form-inline">
											<select class="form-control" style="width:45%">
												<option>MM</option>
												<option>01 - Janiary</option>
												<option>02 - February</option>
												<option>03 - February</option>
											</select>
											<span style="width:10%; text-align: center"> / </span>
											<select class="form-control" style="width:45%">
												<option>YY</option>
												<option>2018</option>
												<option>2019</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV
											<i class="fa fa-question-circle"></i>
										</label>
										<input class="form-control" required="" type="text">
									</div>
									<!-- form-group.// -->
								</div>
							</div>
							<!-- row.// -->
							<button class="subscribe btn btn-primary btn-block" type="button"> Confirm </button>
						</form>
					</div>
					<!-- card-body.// -->
				</article>
				<!-- card.// -->


			</aside>

		</div>
		<!-- row.// -->

	</div>
	<!--container end.//-->



</body>