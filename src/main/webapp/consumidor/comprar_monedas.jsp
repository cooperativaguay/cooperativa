<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%

  //inicializamos las variables loginId y loginName, por el momento sin valor
  int loginId=0;
  String loginName=null;

  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }

    boolean loginRequired=true;

    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...


    if ("POST".equalsIgnoreCase(request.getMethod())) {

  
       request.setCharacterEncoding("UTF-8");
     

    if (request.getParameter("fruiticoins")!=null) {
         if (Float.parseFloat(request.getParameter("fruiticoins"))!=0) {

        float cantidad = Float.parseFloat(request.getParameter("fruiticoins"));
        Consumidor consumidor = ConsumidorController.getId(loginId);

        ConsumidorController.cambiaSaldo(consumidor, true, cantidad);

    
                 
                response.sendRedirect("/cooperativa/consumidor/menu.jsp");
            

                     }
             }
    }


%>

<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta charset="utf-8">
    <title>Canjear monedas</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link rel="stylesheet" href="/cooperativa/css/compras.css">
</head>
<body style="background-color:rgba(102,255,0,0.329)">
    <%@include file="/parts/barra_consumidor.jsp"%>

    <div class="container">

        <div id="cuadro_compramonedas">
            <div class="row">

                <div class="col-md-12 centrando">
                    <input id="valor-eu" type="number" min="0" name="euros" placeholder="Introduce €">
                </div>

                <div class="col-md-12 centrando">
                    <img src="/cooperativa/imagenes/fruity_coin.png" height="200" width="200">
                </div>

                <div class="col-md-6 " style="text-align:center">
                    <button id="convierte-eu-fruit">Cambiar</button>
                    
                </div>

                <div class="col-md-6" style="text-align:center">
                        <form action="#" method="post">
                        <input type="text" name="fruiticoins" id="fruiticoins" >
                        

                    <button type="submit">Comprar fruiticoins</button>
                    </form>
                </div>
                </div>

            </div>
        </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>



        <script>
$(document).on("click","#convierte-eu-fruit", function(){
    fruitis = $("#valor-eu").val()*0.90;
    $("#fruiticoins").val(fruitis);

});

</script>
</body>

</html>