<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%
    boolean loginRequired=true;

    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");

        String nombre = request.getParameter("nombre");
        String contrasena = request.getParameter("contrasena");
        String email = request.getParameter("email");
        String telefono = request.getParameter("telefono");
        String direccion = request.getParameter("direccion");
        float saldo = 0.0f;


        // aquí verificaríamos que todo ok, y solo si todo ok hacemos SAVE, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            Consumidor a = new Consumidor(nombre, contrasena, email, telefono, direccion,  saldo);
            ConsumidorController.save(a);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo alumno
            response.sendRedirect("../index.jsp");
            return;
        }
    }


%>


<!doctype html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Creacion Usuario</title>
  <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
    <link rel="stylesheet" href="/cooperativa/css/crear.css">
</head>

<body style="background-color:rgba(102,255,0,0.329)">
  <form action="#" method="post">
    <h1>CREACION DE USUARIO</h1>
    <br>
    <br>
      <div class="form-group row">
        <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
        <div class="col-sm-10">
          <input class="form-control-plaintext" style="height: 20px" name="nombre" id="nombre" type="text">
        </div>
      </div>

      <div class="form-group row">
          <label for="contrasena" class="col-sm-2 col-form-label">Contraseña</label>
          <div class="col-sm-10">
            <input class="form-control-plaintext" style="height: 20px" name="contrasena" id="contrasena" type="password">
          </div>
        </div>
  

      <div class="form-group row">
        <label for="email" class="col-sm-2 col-form-label">E-mail</label>
        <div class="col-sm-10">
          <input class="form-control-plaintext" style="height: 20px" name="email" id="email" type="text">
        </div>
      </div>

    
      <div class="form-group row">
        <label for="telefono" class="col-sm-2 col-form-label">Telefono</label>
        <div class="col-sm-10">
            <input class="form-control-plaintext" style="height: 20px" id="telefono" name="telefono" type="text">
        </div>
      </div>

      <div class="form-group row">
          <label for="direccion" class="col-sm-2 col-form-label">Direccion</label>
          <div class="col-sm-10">
              <input class="form-control-plaintext" style="height: 20px" id="direccion" name="direccion" type="text">
          </div>
        </div>

        



      </div>
      <br>
      <br>

      <div class="form-row justify-content-center">

          <button type="submit" class="btn btn-success col-4">Dar de alta</button>
      </div>




      </form>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
</body>

</html>