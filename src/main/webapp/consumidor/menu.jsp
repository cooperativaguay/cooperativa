<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>
<%@page import="java.util.List" %>



<%

  //inicializamos las variables loginId y loginName, por el momento sin valor
  int loginId=0;
  String loginName=null;
  Consumidor consumidor = ConsumidorController.getId(loginId);

  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }

  
    String idProvincia = request.getParameter("provincia");
    
 /*
    for (Provinciashasofertas pr : ProvinciashasofertasController.getAll()){
        out.print(pr.getProvinciaid());
    } 
*/
  List<Oferta> listaOfertas = null;
  if (idProvincia==null){    

        listaOfertas = OfertaController.getAll();
    } else {
        int idnumerico = Integer.parseInt(idProvincia);
        listaOfertas = OfertaController.getAllProvincia(idnumerico);
    } 
%>

<!doctype html>
<html lang="es">

</html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ofertas</title>
    <link rel="stylesheet" href="/cooperativa/css/ofertas.css">
    <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">

</head>
<body style="background-color:rgba(102,255,0,0.329)">
    <%@include file="/parts/barra_consumidor.jsp"%> 
    <!-- listado provincias -->

    <div class="container ">
    <br>
        <div class="row">
            <!-- fila contenedor -->
            <div class="col-md-6">
                <!-- columna de ofertas -->

                <!-- escribir aqui el if del provincia -->

                <% for (Oferta o : listaOfertas){%>
                <div class="oferta">
                    <!-- inicio oferta-->
                    <div class="row">
                             
                        <div class="col-12 col-sm-4">
                            <img src="/cooperativa/uploads/<%= o.getImagenid()%>"class=" img-thumbnail foto_oferta" alt="tomate">
                               
                            <div id="proveedor_oferta" style="font-size:12pt;">
                            Proveedor:<% Proveedor pr = ProveedorController.getId(o.getProveedorid()); %>
                       
                               <%out.print(pr.getNombre()); %>
                     
                            </div>
                        
                        </div>
                        <div class="col-12 col-sm-8">
                        <% Producto p = ProductoController.getId(o.getProductoid());
                        
                         %>
                                <h3><%=p.getNombre() %></h3>
                                <hr>
                                <div id="descripcion_oferta">
                                    <%= o.getDescripion() %>
                                </div>
                          </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="precio_oferta">
                            Precio / unidad: <%out.print(o.getPrecio()); %> fruits
                            </div>
                        </div>
                        <div class="col">
                            <div id="boton_oferta">
                            <!-- formulario -->
                            <form action="/cooperativa/consumidor/proceso_pedido.jsp"  method="post">
                                <input type="hidden" name="id_oferta" value="<%=o.getId()%>">
                                <button type="sumbit" class="boton_color">Pedir</button>
                            </form>
                            <!--fin formulario-->
                            </div>
                        </div>
                    </div>

                </div>
                <%};%>
            </div>
            <br>
            <br>
            <div class="col-md-6">
                <div class="row">
                    <div class="scroll">
                        <img src="/cooperativa/imagenes/mapa_comunidades.jpg" alt="mapa_comunidades" usemap="#comunidadesmap">
                    </div>
                    <map name="comunidadesmap">
                        <!-- ¡cambiar coordenadas originales!-->
                        <area shape="circle" coords=140,55,10 alt="A_coruna" href="/cooperativa/consumidor/menu.jsp?provincia=16">
                        <area shape="circle" coords=178,60,10 alt="Lugo" href="/cooperativa/consumidor/menu.jsp?provincia=28">
                        <area shape="circle" coords=129,86,10 alt="Pontevedra" href="/cooperativa/consumidor/menu.jsp?provincia=36">
                        <area shape="circle" coords=172,99,10 alt="Ourense" href="/cooperativa/consumidor/menu.jsp?provincia=33">

                        <area shape="circle" coords=227,55,10 alt="Asturias" href="/cooperativa/consumidor/menu.jsp?provincia=5">
                        <area shape="circle" coords=291,62,10 alt="Cantabria" href="/cooperativa/consumidor/menu.jsp?provincia=12">
                        <area shape="circle" coords=330,66,5 alt="Vizcaya" href="/cooperativa/consumidor/menu.jsp?provincia=48">
                        <area shape="circle" coords=351,70,5 alt="Guipuzcoa" href="/cooperativa/consumidor/menu.jsp?provincia=21">
                        <area shape="circle" coords=335,85,5 alt="Alaba" href="/cooperativa/consumidor/menu.jsp?provincia=1">

                        <area shape="circle" coords=336,105,10 alt="La_Rioja" href="/cooperativa/consumidor/menu.jsp?provincia=37">
                        <area shape="circle" coords=373,50,10 alt="Navarra" href="/cooperativa/consumidor/menu.jsp?provincia=32">
                        <area shape="circle" coords=419,118,10 alt="Huesca" href="/cooperativa/consumidor/menu.jsp?provincia=23">
                        <area shape="circle" coords=377,144,10 alt="Zaragoza" href="/cooperativa/consumidor/menu.jsp?provincia=50">
                        <area shape="circle" coords=387,189,10 alt="Teruel" href="/cooperativa/consumidor/menu.jsp?provincia=44">

                        <area shape="circle" coords=455,129,10 alt="Lleida" href="/cooperativa/consumidor/menu.jsp?provincia=27">
                        <area shape="circle" coords=445,172,10 alt="Tarragona" href="/cooperativa/consumidor/menu.jsp?provincia=42">
                        <area shape="circle" coords=484,146,10 alt="Barcelona" href="/cooperativa/consumidor/menu.jsp?provincia=8">
                        <area shape="circle" coords=513,123,10 alt="Girona" href="/cooperativa/consumidor/menu.jsp?provincia=18">

                        <area shape="circle" coords=414,208,10 alt="Castellon" href="/cooperativa/consumidor/menu.jsp?provincia=13">
                        <area shape="circle" coords=390,246,10 alt="Valencia" href="/cooperativa/consumidor/menu.jsp?provincia=46">
                        <area shape="circle" coords=392,288,10 alt="Alicante" href="/cooperativa/consumidor/menu.jsp?provincia=3">
                        <area shape="circle" coords=516,238,10 alt="Baleares" href="/cooperativa/consumidor/menu.jsp?provincia=24">

                        <area shape="circle" coords=82,396,15 alt="Las_Palmas" href="/cooperativa/consumidor/menu.jsp?provincia=35">
                        <area shape="circle" coords=24,395,15 alt="Santa_Cruz" href="/cooperativa/consumidor/menu.jsp?provincia=43">
                        <area shape="circle" coords=198,264,15 alt="Badajoz" href="/cooperativa/consumidor/menu.jsp?provincia=7">
                        <area shape="circle" coords=204,214,15 alt="Caceres" href="/cooperativa/consumidor/menu.jsp?provincia=10">

                        <area shape="circle" coords=277,222,15 alt="Toledo" href="/cooperativa/consumidor/menu.jsp?provincia=45">
                        <area shape="circle" coords=284,262,15 alt="Ciudad_Real" href="/cooperativa/consumidor/menu.jsp?provincia=14">
                        <area shape="circle" coords=345,269,15 alt="Albacete" href="/cooperativa/consumidor/menu.jsp?provincia=2">
                        <area shape="circle" coords=343,220,15 alt="Cuenca" href="/cooperativa/consumidor/menu.jsp?provincia=17">
                        <area shape="circle" coords=333,178,15 alt="Guadalajara" href="/cooperativa/consumidor/menu.jsp?provincia=20">
                        <area shape="circle" coords=291,190,15 alt="Madrid"  href="/cooperativa/consumidor/menu.jsp?provincia=29">

                        <area shape="circle" coords=360,310,15 alt="Murcia" href="/cooperativa/consumidor/menu.jsp?provincia=31">
                        <area shape="circle" coords=331,342,15 alt="Almeria" href="/cooperativa/consumidor/menu.jsp?provincia=4">
                        <area shape="circle" coords=291,340,15 alt="Granada" href="/cooperativa/consumidor/menu.jsp?provincia=19">
                        <area shape="circle" coords=289,303,15 alt="Jaen" href="/cooperativa/consumidor/menu.jsp?provincia=25">
                        <area shape="circle" coords=245,301,15 alt="Cordoba" href="/cooperativa/consumidor/menu.jsp?provincia=15">
                        <area shape="circle" coords=208,321,15 alt="Sevilla" href="/cooperativa/consumidor/menu.jsp?provincia=40">
                        <area shape="circle" coords=244,352,15 alt="Malaga" href="/cooperativa/consumidor/menu.jsp?provincia=30">
                        <area shape="circle" coords=202,361,15 alt="Cadiz" href="/cooperativa/consumidor/menu.jsp?provincia=11">
                        <area shape="circle" coords=167,313,15 alt="Huelva" href="/cooperativa/consumidor/menu.jsp?provincia=22">

                        <area shape="circle" coords=209,167,15 alt="Salamanca" href="/cooperativa/consumidor/menu.jsp?provincia=38">
                        <area shape="circle" coords=254,178,15 alt="Avila" href="/cooperativa/consumidor/menu.jsp?provincia=6">
                        <area shape="circle" coords=283,156,10 alt="Segovia" href="/cooperativa/consumidor/menu.jsp?provincia=39">
                        <area shape="circle" coords=336,140,15 alt="Soria" href="/cooperativa/consumidor/menu.jsp?provincia=41">
                        <area shape="circle" coords=301,105,15 alt="Burgos" href="/cooperativa/consumidor/menu.jsp?provincia=9">
                        <area shape="circle" coords=270,101,10 alt="Palencia" href="/cooperativa/consumidor/menu.jsp?provincia=34">
                        <area shape="circle" coords=228,86,15 alt="Leon" href="/cooperativa/consumidor/menu.jsp?provincia=26">
                        <area shape="circle" coords=221,125,15 alt="Zamora" href="/cooperativa/consumidor/menu.jsp?provincia=49">
                        <area shape="circle" coords=256,136,15 alt="Valladolid" href="/cooperativa/consumidor/menu.jsp?provincia=47">
                    </map>
                </div>
                <div class="row">
                    <div class="saldo">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Saldo disponible</label>
                            <br>
                            <spam>
                            <% Consumidor co = ConsumidorController.getId(loginId); %>
                            <%=co.getSaldo() %>
                            </spam>
                        </div>
                    </div>
                </div>
                <!-- fila contenedor -->
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
</body>

</html>