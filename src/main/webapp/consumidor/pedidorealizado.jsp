<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>
<%@page import="java.util.Date" %>


<%

 int loginId=0;
  String loginName=null;
  boolean ok = false;

  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }


    boolean loginRequired=true;

    boolean datosOk;

        int mensaje = 0;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");

        int ofertaid = Integer.parseInt(request.getParameter("idoferta"));
        int cantidad = Integer.parseInt(request.getParameter("cantidad"));

        Oferta oferta = OfertaController.getId(ofertaid);
        
        ok = OfertaController.convierteEnPedido(oferta, cantidad, loginId);
   
       
       }


%>



<!doctype html>
<html lang="es">

</html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ofertas</title>
    <link rel="stylesheet" href="/cooperativa/css/ofertas.css">
    <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">

</head>
<body style="background-color:rgba(102,255,0,0.329)">
    <%@include file="/parts/barra_consumidor.jsp"%>
    <div class="container ">

   <% if (ok==true) { %> 
   <div class="pedido_realizado">
       <div class="alert alert-warning" style="text-align=center" role="alert">
            <h4 class="alert-heading">¡Pedido realizado con éxito!</h4>
               <p> Su pedido se ha guardado en nuestra base de datos.</p>
           <hr>
                <p class="mb-0 pedido_realizado">Vuelva a su perfil para realizar más pedidos</p>
         </div>
    </div>
   
    <% }
   
   else {%>     
   
   <div class="pedido_realizado">
         <div class="alert alert-danger" style="text-align=center" role="alert">
              <h4 class="alert-heading">¡Su pedido no ha podido efectuarse!</h4>
                 <p> Se ha sobrepasado la cantidad máxima </p>
          <hr>
          <p class="mb-0 pedido_realizado">Vuelva a su perfil para realizar más pedidos</p>
        </div>
    </div>
    
    <%}%>
        
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
</body>

</html>