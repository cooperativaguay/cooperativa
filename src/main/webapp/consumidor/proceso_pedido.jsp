﻿<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>
<%@page import="java.util.List" %>

<%
    int loginId=0;
    String loginName=null;
    boolean ok = false;

    int idoferta = 0;
    Oferta oferta = null;
   
  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");

        idoferta = Integer.parseInt(request.getParameter("id_oferta"));
         oferta = OfertaController.getId(idoferta);

    }
    %>


<!doctype html>
<html lang="es">

</html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ofertas</title>
    <link rel="stylesheet" href="/cooperativa/css/sobre_nosotros.css">
    <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">
</head>
<body style="background-color:rgba(102,255,0,0.329)">
    <%@include file="/parts/barra_consumidor.jsp"%>
    <br>
        <div class="container">
            <div class="row">
                <div class="foto col-2">
                <img id="imagenpedido" src="/cooperativa/uploads/<%= oferta.getImagenid()%>" class=" img-thumbnail foto_oferta" alt="tomate">
                <%Proveedor pr = ProveedorController.getId(oferta.getProveedorid()); %>
                <hr>
                <b>PROVEEDOR:</b><br> 
                <%=pr.getNombre()%>
                </div>
                <div class="descripcion col-7">
                        <h2>
                        <% Producto p = ProductoController.getId(oferta.getProductoid()); %>
                            <%=p.getNombre()%>
                        </h2>
                    <hr>
                    <hr>
                    <h4><b>Descripción del producto:</b></h4>
                    <%=oferta.getDescripion()%>
                    <br>
                    <br>
                    <h5><b>Fecha de envío: </h5></b>
                    <%=oferta.getFechaenvio()%>
                </div>
                <div class="saldo col-3">
                    <h3>SALDO</h3>
                    <% Consumidor co = ConsumidorController.getId(loginId); %>
                    <%=co.getSaldo() %> Fruiticoins
                     <p><b>Unidades restantes: </b> <%=oferta.getCantidadmax() - oferta.cantidadPedida()%></p>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <form action="/cooperativa/consumidor/pedidorealizado.jsp" method="post">
                            <input type="hidden" id="idoferta" name="idoferta" value="<%=idoferta%>">
                            <label for="cantidadmin">Cantidad:</label>
                            <input type="number" min="0" id="cantidad" name="cantidad"><br>
                            <button type="submit" class="btn btn-success btn-block">Pedir</button>
                    </form> 
                </div>
                <div class="precio col-2">
                    <hr>
                    <b>Precio / unidad: </b>
                    <%=oferta.getPrecio()%> fruiticoins
                </div>
                <div class="descripcion2 col-7">
                    <p><b>Fecha tope de la oferta:  </b> <%=oferta.getFechatope()%> </p>
                </div>

            </div>    
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
</body>

</html>