<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%
    String id = request.getParameter("id");
    if (id==null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/cooperativa/index.jsp");
    } else {
        int id_numerico = Integer.parseInt(id);
        PedidoController.removeId(id_numerico);
        response.sendRedirect("/cooperativa/consumidor/pedidos_compras.jsp");
    }

%>
