<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>


<%
/*
LOGIN.JSP - Módulo "incrustable"
contiene menú modal donde se solicta el nombre de usuario o email y el password
el form envía via post el resultado al JSP el codigo siguiente procesa los datos e intenta el login
en caso que lo consiga, asigna las variables de sesión correspondientes
este LOGIN.JSP debe incrustarse ANTES del MENU.JSP
*/

System.out.println(request.getContextPath());

// si recibimos datos via POST
if ("POST".equalsIgnoreCase(request.getMethod())) {
    request.setCharacterEncoding("UTF-8");

    //miramos si existe el parámetro "loginpost"
   
        String paramName = request.getParameter("nombre");
        String paramPassword = request.getParameter("contrasena");
        //aquí ejecutamos el método que debe verificar nombre y password
        //recibiremos en itemId un id válido >0 o bien -1 (VER METODO)
        int itemId = ConsumidorController.checkLogin(paramName,paramPassword);
        //si hemos recibido id válido, asignamos variables de session
        if (itemId>0) {
            //HttpSession session=request.getSession();
            session.setAttribute("loginName",paramName);
            session.setAttribute("loginId",itemId);
             response.sendRedirect("/cooperativa/consumidor/menu.jsp");
             return;
         } else {
            System.out.println("no id");
         }
} 

Actualiza.actualiza();

%>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/cooperativa/css/login.css">
    <title>Cooperativa</title>


</head>
<body class="sobre_nosotros"> <!--  -->
  <div class="login-page" col-6>
    <div class="form">
      <form class="login-form" method="POST" action="#">
        <div class="usuariotipo">
          <div class="row">
            <div class="col-6">
            <button type="button"><b>Consumidor</b></button>
            </div>
            <div class="col-6">
              <a href="/cooperativa/index1.jsp" target="_self"> <button type="button">Proveedor</button></a>
            </div>
          </div>
        </div>
        <input id="nombre" name="nombre" type="text" placeholder="Usuario"/>
        <input id="contrasena" name="contrasena" type="password" placeholder="Contraseña"/>
        <button type="submit" name="boton"> Login </button>
        <!-- <button>login</button> -->
        <p class="message">Registrate aquí   <a href="/cooperativa/consumidor/create.jsp" target="_self">Crear</a></p>
        <p class="message">Entrar como   <a href="/cooperativa/invitado.jsp" target="_self">Invitado</a></p>
      </form>
    </div>
  </div>
  <video autoplay loop mute id="video_background" preload="auto">
  <source src="videofruta.mp4" type="video/mp4"></source>
  </video>
  <!-- Include the above in your HEAD tag -->
  <!--- Footer -->
  <footer class="footer-bs">
    <div class="row">
      <div class="col-md-4 footer-nav animated fadeInUp" style="text-align: center">
        <h3> — LOGO —</h3>
        <img src="/cooperativa/imagenes/logobueno.JPG" width="150" height="150">
      </div>
      <div class="col-md-4 footer-nav animated fadeInUp" style="text-align: center">
        <h3> — MENU —</h3>
        <div class="col">
          <ul class="list">
            <li>
              <a href="/cooperativa/sobre_nosotros.jsp">Sobre nosotros</a>
            </li>
            <li>
              <a href="/cooperativa/condiciones.jsp">Términos y condiciones</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-4 footer-social animated fadeInDown" style="text-align: center">
        <h3> — SIGUENOS — </h3>
        <ul>
          <li>
            <a href="https://www.facebook.com/Frutadona-2143694799250669/">Facebook</a>
          </li>
          <li>
            <a href="https://twitter.com/frutadona?">Twitter</a>
          </li>
          <li>
            <a href="https://www.instagram.com/delahuertaatucasafrutadona/">Instagram</a>
          </li>
          <li>
            <a href="mailto:delahuertaatucasafrutadona@gmail.com?Subject=Hello%Frutadona" target="_top">Email</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
</body>

</html>