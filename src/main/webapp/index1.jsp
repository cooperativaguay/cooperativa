<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>


<%
/*
LOGIN.JSP - Módulo "incrustable"
contiene menú modal donde se solicta el nombre de usuario o email y el password
el form envía via post el resultado al JSP el codigo siguiente procesa los datos e intenta el login
en caso que lo consiga, asigna las variables de sesión correspondientes
este LOGIN.JSP debe incrustarse ANTES del MENU.JSP
*/

System.out.println(request.getContextPath());
boolean no_existe=false;
// si recibimos datos via POST
if ("POST".equalsIgnoreCase(request.getMethod())) {
    request.setCharacterEncoding("UTF-8");

    //miramos si existe el parámetro "loginpost"
   
        String paramName = request.getParameter("CIF");
        String paramPassword = request.getParameter("contrasena");
        //aquí ejecutamos el método que debe verificar nombre y password
        //recibiremos en itemId un id válido >0 o bien -1 (VER METODO)
        int itemId = ProveedorController.checkLogin(paramName,paramPassword);
        //si hemos recibido id válido, asignamos variables de session
        if (itemId>0) {
            //HttpSession session=request.getSession();
            session.setAttribute("loginName",paramName);
            session.setAttribute("loginId",itemId);
             response.sendRedirect("/cooperativa/proveedor/menu1.jsp");
             return;
         } else {
            System.out.println("no id");
            no_existe=true;

         }
} 

%>
<html lang="es">
<!doctype html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/cooperativa/css/login.css">
    <title>Cooperativa</title>
</head>
<body class="sobre_nosotros"> <!--  -->
  <div class="login-page" col-6>
    <div class="form">
      <form class="login-form" method="POST" action="#">
        <div class="usuariotipo">
          <div class="row">
            <div class="col-6">
                <a href="/cooperativa/index.jsp" target="_self"><button type="button">Consumidor</button></a>
            </div>
            <div class="col-6">
            <button type="button"><b>Proveedor</b></button>
            </div>
          </div>
        </div>
        <input id="CIF" name="CIF" type="text" placeholder="CIF"/>
        <input id="contrasena" name="contrasena" type="password" placeholder="Contraseña"/>
        <div>
        <button type="submit" name="boton"> Login </button>
        <% if(no_existe==true){
            out.print("No existe");
        }%>
        <!-- <button>login</button> -->
        </div>
        <p class="message">Registrate aquí<a href="/cooperativa/proveedor/create.jsp" target="_self">Crear</a></p>
      </form>
    </div>
  </div>
    <video autoplay="autoplay" loop="loop" id="video_background" preload="auto" />
  <source src="videofruta.mp4" type="video/mp4"></source>
  </video>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
</body>

</html>