<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>

<%

int loginId=0;
  String loginName=null;
  Proveedor proveedor = null;

     if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }
      for (Proveedor p : ProveedorController.getAll()) {
        if (p.getId() == loginId) {
          proveedor = p;
        }
      }

    
    String nombre = null;
    String cif = null;
    String email = null;
    String contrasena= null;
    String descripcion = null;
    String provinciaid = null;
    float saldo = 0.00f;

    File file;
    int maxFileSize = 10000 * 1024; // 10 mb tamaño máximo en disco
    int maxMemSize = 5000 * 1024; // 5 mb tamaño maximo en memoria

    boolean muestraForm = true;
    String urlImagen = null;
    String pathImagenes = "/uploads"; // carpeta donde guardaremos las imágenes
    String urlBase = request.getContextPath() + pathImagenes;

    // archivoDestino contendrá el nombre del archivo que crearmos
    String archivoDestino = null;

    // getRealPath nos da la ubicación en disco de la carpeta /uploads
    // necesaria para guardar el archivo subido
    // podria ser cualquier carpeta con acceso de escritura para el usuario "tomcat"
    // en linux
    // o cualquier carpeta no protegida de windows
    // en cualquier caso, las "/" son estilo linux, ex: "c:/usuarios/nombre/uploads"
    // en este caso suponemos que es la carpeta uploads dentro de webapp
    // IMPORTANTE añadir el "/" al final!
    String realPath = pageContext.getServletContext().getRealPath(pathImagenes) + "/";

    // verificamos si estamos recibiendo imagen
    // en dos sentidos: si es POST y si es tipo multipart
    String contentType = request.getContentType();

    if("POST".equalsIgnoreCase(request.getMethod())&&contentType.indexOf("multipart/form-data")>=0)
    {

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(maxMemSize);
        // por si hiciese falta, indicamos la carpeta tmp
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(maxFileSize);
        try {
            // ojo, fileItems no sólo incluye archivos, podrian ser también
            // campos INPUT tales como el pie de foto
            List<FileItem> fileItems = upload.parseRequest(request);

            if (fileItems != null && fileItems.size() > 0) {
                for (FileItem fi : fileItems) {

                    // sólo actuamos si fi NO es un campo input
                    if (fi.isFormField()) {
                      
                      String fieldname = fi.getFieldName();
                      String fieldvalue = fi.getString();
                      CamposProveedor enumval = CamposProveedor.valueOf(fieldname.toUpperCase());

                  

                      switch (enumval) {
                        case NOMBRE:
                          nombre = fieldvalue;
                          break;
                      
                        case CIF :
                          cif = fieldvalue;
                          break;
                        
                        case EMAIL:
                          email = fieldvalue;
                          break;

                        case CONTRASENA:
                          contrasena = fieldvalue;
                          break;

                       case DESCRIPCION:
                          descripcion = fieldvalue;
                          break;

                       case PROVINCIAID:
                          provinciaid = fieldvalue;
                          break;



                      }




                    } else {
                        // filename es el nombre del archivo que se ha subido
                        String fileName = fi.getName();
                        // valores que no utilizamos:
                        // String fieldName = fi.getFieldName();
                        // boolean isInMemory = fi.isInMemory();
                        // long sizeInBytes = fi.getSize();

                        // creamos puntero a archivo ubicado en carpeta real del disco
                        // más nombre de archivoDestino (igual al que se ha subido en este caso)
                        // IMPORTANTE: podríamos cambiar aquí el nombre de archivo destino
                        archivoDestino = fileName;
                        file = new File(realPath + archivoDestino);

                        // qué pasa si archivo ya existe? nos inventamos un prefijo
                        // foto.jpg, 1_foto.jpg, 2_foto.jpg...
                        int t = 0;
                        String archivoTemp = archivoDestino;
                        while (file.exists() && !file.isDirectory()) {
                            archivoTemp = String.valueOf(++t) + "_" + archivoDestino;
                            file = new File(realPath + archivoTemp);
                        }
                        archivoDestino = archivoTemp;

                        // escribimos archivo en disco:
                        fi.write(file);
                        // getContextPath() devuelve nombre app, en este caso: "/academiapp"
                        // guardamos el nombre de la imagen tal como lo requerirá html
                        urlImagen = urlBase + "/" + archivoDestino;
                        // muestraform decide si mostramos el form o la foto
                        muestraForm = false;
                        // guardamos nombre img
                        String imagen = archivoDestino;
                        int provincia = Integer.parseInt(provinciaid);


                        Proveedor a = new Proveedor(loginId, nombre, provincia, contrasena, email, cif, descripcion, saldo,
                                imagen);
                        ProveedorController.save(a);

                    }
                }
                // nos vamos a mostrar la lista, que ya contendrá el nuevo alumno
                response.sendRedirect("../index.jsp");
                return;

            }

        } catch (Exception ex) {
            // error, redirigimos a listado...
            System.out.println(ex);

        }

        response.sendRedirect("/cooperativa/proveedor/create.jsp");
        return;

    }
  


%>


<!doctype html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Creacion Usuario</title>
  <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
    <link rel="stylesheet" href="/cooperativa/css/crear.css">
</head>

<body style="background-color:rgba(102,255,0,0.239)">

<div class="row">


<% if (muestraForm) { %>
<div class="col-md-8">







  <form id=usuario action="#" method="post" enctype="multipart/form-data">
    <h1>Ahora introduce tus datos de proveedor</h1>
    <br>
    <br>
      <div class="form-group row">
        <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
        <div class="col-sm-10">
          <input class="form-control-plaintext" style="height: 20px" id="nombre" name="nombre" type="text">
        </div>
      </div>
      <div class="focrm-group row">
        <label for="CIF" class="col-sm-2 col-form-label">CIF</label>
        <div class="col-sm-10">
          <input class="form-control-plaintext" style="height: 20px" id="CIF" name="CIF" type="text">
        </div>
      </div>
      <div class="form-group row">
        <label for="email" class="col-sm-2 col-form-label">E-mail</label>
        <div class="col-sm-10">
          <input class="form-control-plaintext" style="height: 20px" id="email" name="email" type="text">
        </div>
      </div>
      <div class="form-group row">
        <label for="contrasena" class="col-sm-2 col-form-label">Contraseña</label>
        <div class="col-sm-10">
          <input class="form-control-plaintext" style="height: 20px" id="contrasena" name="contrasena" type="password">
        </div>
      </div>
      <div class="form-group row">
        <label for="provinciaid" class="col-sm-2 col-form-label">Provincia</label>
        <div class="col-sm-10">
            <select class="form-control-plaintext" id="provinciaid" name="provinciaid">
            <% for (Provincia p : ProvinciaController.getAll()) { %>
                <option value="<%=p.getId()%>"> <%=p.getNombre()%>
                  </option>
                  <%}%>
                
            </select>
        </div>
      </div>
      <div class="form-group row">
          <label for="descripcion" class="col-sm-2 col-form-label">Descripcion:</label>
          <div class="col-sm-10">
            <textarea id="descripcion" name="descripcion"></textarea>
          </div>
    </div>
      
      <br>
      <br>




 <div>
    <label for="foto">Sube primero tu imagen de perfil :)</label>
    <input type="file" class="form-control-file" name="foto" id="foto">
  </div>
    

  

</div>
<% } else { %>

<div class="col-md-4">
<img src="<%= urlImagen %>" class="img-fluid" />
<h4><%= archivoDestino %> </h3>
</div>
<% } %>
</div>








      <div class="form-row justify-content-center">
          <button type="submit" class="btn btn-success col-4">Dar de alta</button>
      </div>







      </form>


      

      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
</body>
</html>