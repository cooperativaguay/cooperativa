<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/academiapp/css/estilos.css">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Listado</h1>
</div>
</div>


<div class="row">
<div class="col">


<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Email</th>
      <th scope="col">Teléfono</th>
      <th scope="col">Fecha</th>
      <% if (loginId>0) { %>
        <th scope="col"></th>
        <th scope="col"></th>
      <% } %>
    </tr>
  </thead>
  <tbody>

   <% for (Alumno al : AlumnoController.getAll()) { %>
        <tr>
        <th scope="row"><%= al.getId() %></th>
        <td><%= al.getNombre() %></td>
        <td><%= al.getEmail() %></td>
        <td><%= al.getTelefono() %></td>
        <td><%= al.getAlta() %></td>
        <% if (loginId>0) { %>
          <td><a href="/academiapp/alumno/edit.jsp?id=<%= al.getId() %>">Edita</a></td>
          <td><a href="/academiapp/alumno/remove.jsp?id=<%= al.getId() %>">Elimina</a></td>
        <% } %>
        </tr>
    <% } %>
  </tbody>
</table>

<a href="/academiapp/alumno/create.jsp" class="btn btn-sm btn-danger">Nuevo alumno</a>
</div>
</div>


</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/academiapp/js/scripts.js"></script>


</body>
</html>