<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>
<%

  int loginId=0;
  String loginName=null;
  Proveedor proveedor = null;

    if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }
      for (Proveedor p : ProveedorController.getAll()) {
        if (p.getId() == loginId) {
          proveedor = p;
        }
      }


%>
<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/cooperativa/css/ofertas.css">
    <title>Menu</title>
</head>
<body  style="background-color:rgba(102, 255, 0, 0.329)">
    <%@include file="/parts/barra_proveedor.jsp"%>
<div class="container">
      <div class="row">
      <div class="col-md-5  toppad  pull-right col-md-offset-3 ">
       <br>
      </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">PERFIL DE PROVEEDOR</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="Foto de perfil" class=" img-responsive" src="/cooperativa/uploads/<%= proveedor.getImagen()%>"> </div>
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <% for(Proveedor p : ProveedorController.getAll()){ %>
                      <% if (loginId==p.getId()){ %>
                      <tr>
                        <td>Nombre del consumidor</td>
                        <td>
                          <% out.print(p.getNombre()); %>  
                        </td>
                      </tr>
                      <tr>
                        <td>CIF</td>
                        <td>
                          <% out.print(p.getCif()); %>  
                        </td>
                      </tr>
                      <tr>
                        <td>Descripcion</td>
                        <td>
                          <% out.print(p.getDescripcion());%>
                        </td>
                      </tr>
                         <tr>
                             <tr>
                        <td>Email</td>
                        <td>
                          <% out.print(p.getEmail());%>
                        </td>
                      </tr>
                         <tr>
                             <tr>
                        <td>Saldo</td>
                        <td>
                          <% out.print(p.getSaldo());%>
                        </td>
                      </tr>
                    <% } %>
                    <%  }  %>
                    </tbody>
                  </table>
                  <div>
                    <a href="/cooperativa/proveedor/publicaoferta.jsp" target="_self"> <button class="boton_color">PUBLICAR OFERTA</button></a>
                  </div>
                </div>
              </div>
            </div>
                 <div class="panel-footer">
                        <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                        <span class="pull-right">
                            <a href="/cooperativa/proveedor/edit.jsp" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </span>
                    </div>
            
          </div>
        </div>
      </div>
    </div>     
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
</body>

</html>
