<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%

  int loginId=0;
  String loginName=null;

    if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }

%>
<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/cooperativa/css/login.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">
    <title>Cooperativa</title>
</head>

<body style="background-color:rgba(102, 255, 0, 0.329);">
    <%@include file="/parts/barra_proveedor.jsp"%>
    <br>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="pedidos col-12">
                <table class="table table-hover">
                    <thead>
                        <th>OFERTAS</th>
                        <tr>
                            <th scope="col">Proveedor</th>
                            <th scope="col">Producto</th>
                            <th scope="col">Cantidad mínima</th>
                            <th scope="col">Cantidad máxima</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Fecha envío</th>
                            <th scope="col">Fecha tope</th>
                            <th scope="col">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% for (Oferta o : OfertaController.getAll()) {
                            if (o.getProveedorid()==loginId) {
                                
                             %>
                        <tr>
                            <th scope="row">
                               <%= ProveedorController.getId(o.getProveedorid()).getNombre() %>
                            </th>
                            <td>
                                <% for (Producto p : ProductoController.getAll()) { %>
                                    <% if (o.getProductoid()==p.getId()){ %>
                                        <% out.print(p.getNombre());%>
                                    <% }; %>
                                <% }; %>    
                            </td>
                            <td><% out.print(o.getCantidadmin());%></td>
                            <td><% out.print(o.getCantidadmax());%></td>
                            <td><% out.print(o.getPrecio());%></td>
                            <td><% out.print(o.getDescripion());%></td>
                            <td><% out.print(o.getFechaenvio());%></td>
                            <td><% out.print(o.getFechatope());%></td>
                            <td>
                                <a href="/cooperativa/proveedor/remove.jsp?id=<%= o.getId() %>"<i class="far fa-trash-alt" style="color:rgb(250, 7, 7);"></i></a>
                            </td>
                        </tr>
                        <% } //if proveedor coincide
                        } // bucle for %>
                    </tbody>
                </table>
            </div>


        </div>
    </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
</body>

</html>