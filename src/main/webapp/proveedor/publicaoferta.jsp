<<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>

<%
     int loginId=0;
  String loginName=null;

  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }
      }




        String producto = null;
        String descripcion = null;
        String fechaenvio = null;
        String fechatope = null;
        String cantidadmin =  null;
        String cantidadmax =  null;
        String precio = null;
    
        String provincias= null;
        List<String> listaprovincias = new ArrayList <String>();

        
     

    File file;
    int maxFileSize = 10000 * 1024; // 10 mb tamaño máximo en disco
    int maxMemSize = 5000 * 1024; // 5 mb tamaño maximo en memoria

    boolean muestraForm = true;
    String urlImagen = null;
    String pathImagenes = "/uploads"; // carpeta donde guardaremos las imágenes
    String urlBase = request.getContextPath() + pathImagenes;

    // archivoDestino contendrá el nombre del archivo que crearmos
    String archivoDestino = null;

    // getRealPath nos da la ubicación en disco de la carpeta /uploads
    // necesaria para guardar el archivo subido
    // podria ser cualquier carpeta con acceso de escritura para el usuario "tomcat"
    // en linux
    // o cualquier carpeta no protegida de windows
    // en cualquier caso, las "/" son estilo linux, ex: "c:/usuarios/nombre/uploads"
    // en este caso suponemos que es la carpeta uploads dentro de webapp
    // IMPORTANTE añadir el "/" al final!
    String realPath = pageContext.getServletContext().getRealPath(pathImagenes) + "/";

    // verificamos si estamos recibiendo imagen
    // en dos sentidos: si es POST y si es tipo multipart
    String contentType = request.getContentType();

    if("POST".equalsIgnoreCase(request.getMethod())&&contentType.indexOf("multipart/form-data")>=0)
    {

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(maxMemSize);
        // por si hiciese falta, indicamos la carpeta tmp
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(maxFileSize);
        try {
            // ojo, fileItems no sólo incluye archivos, podrian ser también
            // campos INPUT tales como el pie de foto
            List<FileItem> fileItems = upload.parseRequest(request);

            if (fileItems != null && fileItems.size() > 0) {
                for (FileItem fi : fileItems) {

                    // sólo actuamos si fi NO es un campo input
                    if (fi.isFormField()) {
                      
                      String fieldname = fi.getFieldName();
                      String fieldvalue = fi.getString();
                      CamposOferta enumval = CamposOferta.valueOf(fieldname.toUpperCase());

                  

                      switch (enumval) {
                        case PRODUCTO:
                          producto = fieldvalue;
                          break;
                      
                        case DESCRIPCION :
                          descripcion = fieldvalue;
                          break;
                        
                        case CANTIDADMIN:
                          cantidadmin = fieldvalue;
                          break;

                        case CANTIDADMAX:
                          cantidadmax = fieldvalue;
                          break;

                       case PRECIO:
                          precio = fieldvalue;
                          break;

                        case FECHAENVIO:
                          fechaenvio = fieldvalue;
                          break;

                        case FECHATOPE:
                          fechatope = fieldvalue;
                          break;

                       case PROVINCIAS:
                          provincias = fieldvalue;
                          listaprovincias.add(provincias);
                          break;

            
                      }





                    } else {
                        // filename es el nombre del archivo que se ha subido
                        String fileName = fi.getName();
                        // valores que no utilizamos:
                        // String fieldName = fi.getFieldName();
                        // boolean isInMemory = fi.isInMemory();
                        // long sizeInBytes = fi.getSize();

                        // creamos puntero a archivo ubicado en carpeta real del disco
                        // más nombre de archivoDestino (igual al que se ha subido en este caso)
                        // IMPORTANTE: podríamos cambiar aquí el nombre de archivo destino
                        archivoDestino = fileName;
                        file = new File(realPath + archivoDestino);

                        // qué pasa si archivo ya existe? nos inventamos un prefijo
                        // foto.jpg, 1_foto.jpg, 2_foto.jpg...
                        int t = 0;
                        String archivoTemp = archivoDestino;
                        while (file.exists() && !file.isDirectory()) {
                            archivoTemp = String.valueOf(++t) + "_" + archivoDestino;
                            file = new File(realPath + archivoTemp);
                        }
                        archivoDestino = archivoTemp;

                        // escribimos archivo en disco:
                        fi.write(file);
                        // getContextPath() devuelve nombre app, en este caso: "/academiapp"
                        // guardamos el nombre de la imagen tal como lo requerirá html
                        urlImagen = urlBase + "/" + archivoDestino;
                        // muestraform decide si mostramos el form o la foto
                        muestraForm = false;
                        // guardamos nombre img

                        String imagen = archivoDestino;
                        int idproveedor = loginId;
                        int idproducto= Integer.parseInt(producto);
                        int icantidadmin = Integer.parseInt(cantidadmin);
                        int icantidadmax = Integer.parseInt(cantidadmax);
                        float fprecio = Float.parseFloat(precio);

                        System.out.println(idproducto);
                        System.out.println(imagen);




                        Oferta a = new Oferta(idproveedor, idproducto, imagen, icantidadmin,  icantidadmax, fprecio, descripcion, MiLib.stringToDate(fechaenvio), MiLib.stringToDate(fechatope));
                        OfertaController.save(a);
                        
                        System.out.println(a.getId());

                         for(String provincia : listaprovincias){
                           System.out.println(Integer.parseInt(provincia));  

                         Provinciashasofertas p = new Provinciashasofertas (Integer.parseInt(provincia), a.getId());
                         ProvinciashasofertasController.save(p);}
                    }
                }
                // nos vamos a mostrar la lista, que ya contendrá el nuevo alumno
                response.sendRedirect("/cooperativa/proveedor/menu1.jsp");
                return;

            }

        } catch (Exception ex) {
            // error, redirigimos a listado...
            System.out.println(ex);

        }

        response.sendRedirect("/cooperativa/proveedor/publicaoferta.jsp");
        return;

    }
  


%>


<!doctype html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Publica oferta</title>
  <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
    <link rel="stylesheet" href="/cooperativa/css/crear.css">
</head>

<body style="background-color:rgba(102,255,0,0.329)">

<% if (muestraForm) { %>
    <div class="container">
         <form id=usuario action="#" method="post" enctype="multipart/form-data">

            <div class="form-group">
                <h1>Publicar nueva oferta</h1>
                <div class="form-row">
                    <div class="col-md-8 mb-6">
                        <label for="Producto">Producto</label>
                        <select class="form-control" name="producto" id="Producto">

                                 <% for (Producto p : ProductoController.getAll()) { %>
 
                                     <option value="<%= p.getId() %>"><%= p.getNombre() %></option>
                                     <% } %>


            
                        </select>
                    </div>
                

                <div class="form-row">
                    <label for="descripcion">Descripcion</label>
                    <textarea class="form-control" name="descripcion" id="descripcion" rows="7" placeholder="Haz una descripción de tu producto :)
Indica kg / unidad"></textarea>
                </div>

                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="cantidadmin">Cantidad mínima:</label>
                        <input type="number" min="0" id="cantidadmin" name="cantidadmin">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="cantidadmax">Cantidad máxima:</label>
                        <input type="number" min="0" id="cantidadmax" name="cantidadmax">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="precio">Precio/Unidad:</label>
                        <input type="number" min="0" step="0.01" id="precio" name="precio">
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-6 mb-4">
                        <label for="fechaenvio">Fecha de envío:</label>
                        <input type="date" id="cantidadmin" name="fechaenvio">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="fechatope">Fecha tope de la oferta:</label>
                        <input type="date" id="fechatope" name="fechatope">
                    </div>

                </div>

                 <div class="form-row">
                    <label for="provincias">Provincias</label>
                    <select style="height: 200px" multiple class="form-control" id="provincias" name="provincias"> 
                         <% for (Provincia p : ProvinciaController.getAll()) { %>
 
                                     <option value="<%= p.getId() %>">

                                     <%=p.getNombre() %>
                                      
                                      </option>
                                     <% } %>



                                      
                    </select>

                </div>
             

             
 <div>
    <label for="foto">Sube una imagen de tu producto:</label>
    <input type="file" class="form-control-file" name="foto" id="foto">
  </div>
    

  

</div>
<% } else { %>

<div class="col-md-4">
<img src="<%= urlImagen %>" class="img-fluid" />
<h4><%= archivoDestino %> </h3>
</div>
<% } %>
</div>


            <div class="form-row justify-content-center">

                <button type="submit" class="btn btn-success col-4">Sign in</button>
            </div>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>


</body>

</html>