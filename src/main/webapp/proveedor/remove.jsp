<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%
    String id = request.getParameter("id");
    if (id==null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/cooperativa/index.jsp");
    } else {
        int id_numerico = Integer.parseInt(id);
        OfertaController.removeId(id_numerico);
        ProvinciashasofertasController.removebyOferta(id_numerico);
        
        response.sendRedirect("/cooperativa/proveedor/mis_ofertas.jsp");
    }

%>
