<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="/cooperativa/css/sobre_nosotros.css">
    <link rel="stylesheet" href="https://fonts.google.com/?selection.family=Signika">
    <link rel="stylesheet" href="https://fonts.google.com/?selection.family=Quicksand">

    <title>Quienes somos</title>
</head>


<!-- Include the above in your HEAD tag -->

<body style="background-color: palegreen">
    <div class="container" >
    <div class=" row ">
        <div class="col-sm-12 heading-title text-center">
            <h3 class="text-uppercase ">Nuestro equipo </h3>
            <hr>
            <p class="p-top-30 half-txt ">
                Este proyecto surge en 2018 fruto de la inquietud de un grupo
                de personas preocupadas por la calidad y el precio de las frutas y verduras
                Nuestra filosofía es que tanto productor como consumidor obtengan un precio justo
                y la mejor calidad posible. Esta aplicación ha sido posible gracias a la colaboración
                de productores locales y a personas que comparten las mismas preocupaciones.
            </p>
            <hr>
        </div>

        <div class="col-sm-6 ">
            <div class="team-member " style="text-align: center">
                <div class="team-img ">
                    <img src="imagenes/Laura_fruta.gif " alt="team member " width="200 " height="200 " class="img-responsive ">
                </div>
                <div class="team-hover ">
                    <div class="desk ">
                        <h4>Diseño Java</h4>
                    </div>
                </div>
            </div>
            <div class="team-title " style="text-align: center">
                <h5>Laura Goenaga</h5>
            </div>
        </div>
        <div class="col-sm-6 ">
            <div class="team-member " style="text-align: center">
                <div class="team-img ">
                    <img src="imagenes/victor_fruta.gif " alt="team member " width="200 " height="200 " class="img-responsive ">
                </div>
                <div class="team-hover ">
                    <div class="desk ">
                        <h4>Diseño BackEnd</h4>
                    </div>
                </div>
            </div>
            <div class="team-title " style="text-align: center">
                <h5>Victor Tabarés</h5>
            </div>
        </div>
        <div class="col-sm-6 ">
            <div class="team-member " style="text-align: center">
                <div class="team-img ">
                    <img src="imagenes/maria_fruta.gif" alt="team member " width="200 " height="200 " class="img-responsive ">
                </div>
                <div class="team-hover ">
                    <div class="desk ">
                        <h4>Diseño Bases de datos</h4>
                    </div>
                </div>
            </div>
            <div class="team-title " style="text-align: center">
                <h5>María Velasco</h5>
            </div>
        </div>

        <div class="col-sm-6 ">
            <div class="team-member " style="text-align: center">
                <div class="team-img ">
                    <img src="imagenes/adolfo_fruta.gif " alt="team member " width="200 " height="200 " class="img-responsive ">
                </div>
                <div class="team-hover ">
                    <div class="desk ">
                        <h4>Diseño web FrontEnd</h4>
                    </div>
                </div>
            </div>
            <div class="team-title " style="text-align: center">
                <h5>Adolfo Fernández</h5>
            </div>
        </div>

    </div>

</div>

</body>

</html>